package xyz.zhangyi.diamond.demo.ordercontext.north.local.appservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.zhangyi.diamond.demo.foundation.exception.ApplicationException;
import xyz.zhangyi.diamond.demo.foundation.exception.DomainException;
import xyz.zhangyi.diamond.demo.foundation.stereotype.Local;
import xyz.zhangyi.diamond.demo.ordercontext.south.port.publishers.OrderPlacedEventPublisher;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.Order;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.OrderService;
import xyz.zhangyi.diamond.demo.ordercontext.south.message.OrderPlaced;
import xyz.zhangyi.diamond.demo.ordercontext.north.message.PlacingOrderRequest;

@Service
@Local
public class OrderAppService { // facade service
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderPlacedEventPublisher orderPlacedEventPublisher;

    private static final Logger logger = LoggerFactory.getLogger(OrderAppService.class);

    @Transactional(rollbackFor = ApplicationException.class) // 5. handle transaction
    public void placeOrder(PlacingOrderRequest request) {
        try {
            // 1. validate message

            // 2. convert between message and domain object
            Order order = request.to();
            orderService.placeOrder(order); // domain logic not in application service

            OrderPlaced orderPlaced = OrderPlaced.from(order);
            orderPlacedEventPublisher.publish(orderPlaced);
        } catch (DomainException ex) { // 4. catch exception
            // 3. logging
            logger.warn(ex.getMessage());
            throw new ApplicationException(ex.getMessage(), ex);
        }
    }
}
