package xyz.zhangyi.diamond.demo.ordercontext.domain.order;

public class OrderItem {
    private String id;
    private int quantity;
    private Product purchasedProduct;

    public OrderItem(String id, Product purchasedProduct) {
        this.id = id;
        this.purchasedProduct = purchasedProduct;
    }

    public String id() {
        return id;
    }

    public Product purchased() {
        return purchasedProduct;
    }

    public double price() {
        return purchasedProduct.getPrice() * quantity;
    }
}
