package xyz.zhangyi.diamond.demo.ordercontext.south.port.repositories;

import org.springframework.stereotype.Repository;
import xyz.zhangyi.diamond.demo.foundation.stereotype.Port;
import xyz.zhangyi.diamond.demo.foundation.stereotype.PortType;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.Order;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.OrderId;

import java.util.List;
import java.util.Optional;

@Repository
@Port(PortType.Repository)
public interface OrderRepository {
    Optional<Order> orderOf(OrderId orderId);
    List<Order> all();
    void add(Order order); // regard domain object as persistence object
    void save(Order order);
    void remove(OrderId orderId);
}
