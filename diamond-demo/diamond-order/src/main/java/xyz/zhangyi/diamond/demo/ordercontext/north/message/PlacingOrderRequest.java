package xyz.zhangyi.diamond.demo.ordercontext.north.message;

import java.io.Serializable;

import xyz.zhangyi.diamond.demo.foundation.stereotype.Direction;
import xyz.zhangyi.diamond.demo.foundation.stereotype.MessageContract;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.Order;

@MessageContract(Direction.North)
public class PlacingOrderRequest implements Serializable {
    private static final long serialVersionUID = 6247530014194471997L;
    private String customerId;
    private String country;
    private String city;

    public Order to() {
        return new Order();
    }
}
