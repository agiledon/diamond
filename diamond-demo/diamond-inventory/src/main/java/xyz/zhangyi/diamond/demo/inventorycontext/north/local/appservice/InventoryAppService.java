package xyz.zhangyi.diamond.demo.inventorycontext.north.local.appservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.zhangyi.diamond.demo.inventorycontext.domain.InventoryReview;
import xyz.zhangyi.diamond.demo.inventorycontext.domain.InventoryService;
import xyz.zhangyi.diamond.demo.inventorycontext.domain.PurchasedProduct;
import xyz.zhangyi.diamond.demo.inventorycontext.north.message.CheckingInventoryRequest;
import xyz.zhangyi.diamond.demo.inventorycontext.north.message.InventoryReviewResponse;

import java.util.List;

@Service
public class InventoryAppService {
    @Autowired
    private InventoryService inventoryService;

    public InventoryReviewResponse checkInventory(CheckingInventoryRequest request) {
        List<PurchasedProduct> products = request.to();
        InventoryReview inventoryReview = inventoryService.reviewInventory(products);
        return InventoryReviewResponse.from(inventoryReview);
    }
}
